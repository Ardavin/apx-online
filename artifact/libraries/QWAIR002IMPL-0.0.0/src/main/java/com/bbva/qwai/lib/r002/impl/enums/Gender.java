package com.bbva.qwai.lib.r002.impl.enums;

public enum Gender {

    MALE("V"),FEMALE("H");
	
	private String genderId;
	  
	private Gender(String gender)
	{
	    this.genderId = gender;
	}
	  
	public final String getGenderId()
	{
	    return this.genderId;
	}
	

}
