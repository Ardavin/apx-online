package com.bbva.qwai.lib.r002.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bbva.qwai.lib.r002.impl.enums.*;
import com.bbva.qwai.lib.r002.QWAIR002;

public class QWAIR002Impl extends QWAIR002Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR002.class);
	private static final String MESSAGE_ERROR = "Campo con valor incorrecto: {}";
	private static final String NULL_ERROR = "Dato de entrada nulo";

	/**Se valida que el dato ingresado para el DocType cumpla con cualquiera de estos valores
	 * DNI
	 * CIF
	 * PASSPORT
	 * RESIDENTIAL_PASS
	 * NIF
	 * RUC
	 * CURP
	 * RUT
	 * INE
	 * DL
	 */
	@Override
	public String executeDtoToBdDocType(String documentType) {
		String docType;
		try{
			DocumentType docTypeEnum = DocumentType.valueOf(documentType);
			docType = docTypeEnum.getDocumentCode();
		}
		catch(IllegalArgumentException iae){
			LOGGER.error(MESSAGE_ERROR,documentType);
			LOGGER.error("Argumento invalido en metodo executeDtoToBdDocType {}",iae.getMessage());
			docType="";
		}
		catch(NullPointerException npe){
			LOGGER.error("Argumento null en metodo executeDtoToBdDocType {}",npe.getMessage());
			LOGGER.error(NULL_ERROR);
			docType="";
		}
		return docType;
	}
	/**
	 * Metodo para validar que el codigo de document type devuelto por la BD corresponda al DTO
	 * Devuelve el String correspondiente al codigo
	 */
	@Override
	public String executeBdToDtoDocType(String codTipident) {
		String documentType = "";
		for(DocumentType document:DocumentType.values()){
			if(document.getDocumentCode().equals(codTipident)){
				documentType = document.name();
				break;
			}
		}
		return documentType;
	}

	/**Se valida que el dato ingresado para el Género cumpla con cualquiera de estos valores
	 * MALE
	 * FAMALE
	 */
	@Override
	public String executeDtoToBdGender(String gender) {
		String genderId;
		try{
			Gender genderEnum = Gender.valueOf(gender);
			genderId = genderEnum.getGenderId();
		}
		catch(IllegalArgumentException iae){
			LOGGER.error("Argumento invalido en metodo executeDtoToBdGender {}",iae.getMessage());
			LOGGER.error(MESSAGE_ERROR,gender);
			genderId = "";
		}
		catch(NullPointerException npe){
			LOGGER.error("Argumento null en metodo executeDtoToBdGender {}",npe.getMessage());
			LOGGER.error(NULL_ERROR);
			genderId = "";
		}
		return genderId;
	}
	/**
	 * Metodo para validar que el codigo de genero devuelto por la BD corresponda al DTO
	 * Devuelve el String correspondiente al codigo
	 */
	@Override
	public String executeBdToDtoGender(String xtiSexo) {
		String gender = "";
		for(Gender genderValue:Gender.values()){
			if(genderValue.getGenderId().equals(xtiSexo)){
				gender = genderValue.name();
				break;
			}
		}
		return gender;
	}

	/**Se valida que el dato ingresado para el PersonalTitle cumpla con cualquiera de estos valores
	 * MR
	 * MRS
	 * DR
	 * MISS
	 * MS
	 * REV
	 * SIR_MADAM
	 */
	@Override
	public String executeDtoToBdPersTitle(String personalTittle) {
		String personalTitleId;
		try{
			PersonalTitle titleEnum = PersonalTitle.valueOf(personalTittle);
			personalTitleId = titleEnum.getPersonalTitleCode();
		}
		catch(IllegalArgumentException iae){
			LOGGER.error("Argumento invalido en metodo executeDtoToBdPersTitle {}",iae.getMessage());
			LOGGER.error(MESSAGE_ERROR,personalTittle);
			personalTitleId = "";
		}
		catch(NullPointerException npe){
			LOGGER.error("Argumento null en metodo executeDtoToBdPersTitle {}",npe.getMessage());
			LOGGER.error(NULL_ERROR);
			personalTitleId = "";
		}
		return personalTitleId;
	}
	/**
	 * Metodo para validar que el codigo de personal title devuelto por la BD corresponda al DTO
	 * Devuelve el String correspondiente al codigo
	 */
	@Override
	public String executeBdToDtoPersTitle(String codTratanor) {
		String personalTitle = "";
		for(PersonalTitle title:PersonalTitle.values()){
			if(title.getPersonalTitleCode().equals(codTratanor)){
				personalTitle = title.name();
				break;
			}
		}
		return personalTitle;
	}

	/**Se valida que el dato ingresado para el MaritalStatus cumpla con cualquiera de estos valores
	 * MARRIED
	 * SINGLE
	 * WIDOWED
	 * CIVIL_UNION_AGREEMENT_WITH_SEPARATED_PROPERTY
	 * CIVIL_UNION_AGREEMENT_WITHOUT_SEPARATED_PROPERTY
	 * COHABITANT
	 */
	@Override
	public String executeDtoToBdMaritalSt(String maritalStatus) {
		String maritalStatusId;
		try{
			MaritalStatus maritalStatusEnum = MaritalStatus.valueOf(maritalStatus);
			maritalStatusId = maritalStatusEnum.getMaritalStatusId();
		}
		catch(IllegalArgumentException iae){
			LOGGER.error("Argumento invalido en metodo executeDtoToBdMaritalSt {}",iae.getMessage());
			LOGGER.error(MESSAGE_ERROR,maritalStatus);
			maritalStatusId = "";
		}
		catch(NullPointerException npe){
			LOGGER.error("Argumento invalido en metodo executeDtoToBdMaritalSt {}",npe.getMessage());
			LOGGER.error(NULL_ERROR);
			maritalStatusId = "";
		}
		return maritalStatusId;
		
		
	}
	/**
	 * Metodo para validar que el codigo de estado civil devuelto por la BD corresponda al DTO
	 * Devuelve el String correspondiente al codigo
	 */
	@Override
	public String executeBdToDtoMaritalSt(String codCecivi) {
		String maritalStatus = "";
		for(MaritalStatus maritalSt:MaritalStatus.values()){
			if(maritalSt.getMaritalStatusId().equals(codCecivi)){
				maritalStatus = maritalSt.name();
				break;
			}
		}
		return maritalStatus;
	}
}