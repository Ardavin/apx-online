package com.bbva.qwai.lib.r002.impl.enums;

public enum PersonalTitle {

	DR("42"),MR("15"), MRS("16"), MISS("47"), MS("48"), REV("12"), SIR_MADAM("27");
	
	private String personalTitleCode;
	  
	private PersonalTitle(String personalTitle)
	{
	    this.personalTitleCode = personalTitle;
	}
	  
	public final String getPersonalTitleCode()
	{
	    return this.personalTitleCode;
	}

}
