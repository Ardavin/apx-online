package com.bbva.qwai.lib.r002.impl.enums;

public enum MaritalStatus {
	
	MARRIED("C"),SINGLE("S"), WIDOWED("V"), CIVIL_UNION_AGREEMENT_WITH_SEPARATED_PROPERTY ("U"), CIVIL_UNION_AGREEMENT_WITHOUT_SEPARATED_PROPERTY("W"),COHABITANT("H");
	
	private String maritalStatusCode;
	  
	private MaritalStatus(String maritalStatus)
	{
	    this.maritalStatusCode = maritalStatus;
	}
	  
	public final String getMaritalStatusId()
	{
	    return this.maritalStatusCode;
	}
	

}
