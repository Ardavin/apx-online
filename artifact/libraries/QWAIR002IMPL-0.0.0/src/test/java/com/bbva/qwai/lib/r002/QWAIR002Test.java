package com.bbva.qwai.lib.r002;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;

import javax.annotation.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/QWAIR002-app.xml",
		"classpath:/META-INF/spring/QWAIR002-app-test.xml",
		"classpath:/META-INF/spring/QWAIR002-arc.xml",
		"classpath:/META-INF/spring/QWAIR002-arc-test.xml" })
public class QWAIR002Test {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(QWAIR002.class);
	
	@Resource(name = "qwaiR002")
	private QWAIR002 qwaiR002;
	
	@Before
	public void setUp() throws Exception {		
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.qwaiR002;
		if(this.qwaiR002 instanceof Advised){
			Advised advised = (Advised) this.qwaiR002;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}
	
	@Test
	public void executeDtoToBdDocType(){
		LOGGER.debug("Ejecutando test executeDtoToBdDocType");
		String codigo = qwaiR002.executeDtoToBdDocType("INE");
		LOGGER.debug("Codigo recibido {} para el valor {}", codigo,"INE");
		Assert.assertNotSame("", codigo);
	}
	@Test
	public void executeBdToDtoDocType(){
		LOGGER.debug("Ejecutando test executeBdToDtoDocType");
		String texto = qwaiR002.executeBdToDtoDocType("1");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertNotSame("", texto);
	}
	@Test
	public void executeDtoToBdDocTypeNoValid(){
		LOGGER.debug("Ejecutando test executeDtoToBdDocTypeNoValid");
		String codigo = qwaiR002.executeDtoToBdDocType("NVD");
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeDtoToBdDocTypeNull(){
		LOGGER.debug("Ejecutando test executeDtoToBdDocTypeNull");
		String codigo = qwaiR002.executeDtoToBdDocType(null);
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeBdToDtoDocTypeNoValid(){
		LOGGER.debug("Ejecutando test executeBdToDtoDocTypeNoValid");
		String texto = qwaiR002.executeBdToDtoDocType("100");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertEquals("", texto);
	}
	
	@Test
	public void executeDtoToBdGender(){
		LOGGER.debug("Ejecutando test executeDtoToBdGender");
		String codigo = qwaiR002.executeDtoToBdGender("FEMALE");
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertNotSame("", codigo);
	}
	@Test
	public void executeBdToDtoGender(){
		LOGGER.debug("Ejecutando test executeBdToDtoGender");
		String texto = qwaiR002.executeBdToDtoGender("H");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertNotSame("", texto);
	}
	
	@Test
	public void executeDtoToBdGenderNoValid(){
		LOGGER.debug("Ejecutando test executeDtoToBdGenderNoValid");
		String codigo = qwaiR002.executeDtoToBdGender("FEMENINO");
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeDtoToBdGenderNull(){
		LOGGER.debug("Ejecutando test executeDtoToBdGenderNull");
		String codigo = qwaiR002.executeDtoToBdGender(null);
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeBdToDtoGenderNoValid(){
		LOGGER.debug("Ejecutando test executeBdToDtoGenderNoValid");
		String texto = qwaiR002.executeBdToDtoGender("F");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertEquals("", texto);
	}
	
	@Test
	public void executeDtoToBdPersTitle(){
		LOGGER.debug("Ejecutando test executeDtoToBdPersTitle");
		String codigo = qwaiR002.executeDtoToBdPersTitle("MISS");
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertNotSame("", codigo);
	}
	@Test
	public void executeBdToDtoPersTitle(){
		LOGGER.debug("Ejecutando test executeBdToDtoPersTitle");
		String texto = qwaiR002.executeBdToDtoPersTitle("47");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertNotSame("", texto);
	}
	@Test
	public void executeDtoToBdPersTitleNoValid(){
		LOGGER.debug("Ejecutando test executeDtoToBdPersTitleNoValid");
		String codigo = qwaiR002.executeDtoToBdPersTitle("SEÑORA");
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeDtoToBdPersTitleNull(){
		LOGGER.debug("Ejecutando test executeDtoToBdPersTitleNull");
		String codigo = qwaiR002.executeDtoToBdPersTitle(null);
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeBdToDtoPersTitleNoValid(){
		LOGGER.debug("Ejecutando test executeBdToDtoPersTitleNoValid");
		String texto = qwaiR002.executeBdToDtoPersTitle("10");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertEquals("", texto);
	}
	@Test
	public void executeDtoToBdMaritalSt(){
		LOGGER.debug("Ejecutando test executeDtoToBdMaritalSt");
		String codigo = qwaiR002.executeDtoToBdMaritalSt("SINGLE");
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertNotSame("", codigo);
	}
	@Test
	public void executeBdToDtoMaritalSt(){
		LOGGER.debug("Ejecutando test executeBdToDtoMaritalSt");
		String texto = qwaiR002.executeBdToDtoMaritalSt("U");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertNotSame("", texto);
	}
	@Test
	public void executeDtoToBdMaritalStNoValid(){
		LOGGER.debug("Ejecutando test executeDtoToBdMaritalStNoValid");
		String codigo = qwaiR002.executeDtoToBdMaritalSt("SOLTERA");
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeDtoToBdMaritalStNull(){
		LOGGER.debug("Ejecutando test executeDtoToBdMaritalStNull");
		String codigo = qwaiR002.executeDtoToBdMaritalSt(null);
		LOGGER.debug("Codigo recibido {}", codigo);
		Assert.assertEquals("", codigo);
	}
	@Test
	public void executeBdToDtoMaritalStNoValid(){
		LOGGER.debug("Ejecutando test executeBdToDtoMaritalStNoValid");
		String texto = qwaiR002.executeBdToDtoMaritalSt("ST");
		LOGGER.debug("Texto recibido {}", texto);
		Assert.assertEquals("", texto);
	}
}