package com.bbva.qwai.lib.r001.impl;

import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;
import com.bbva.qwai.lib.r002.QWAIR002;
import com.bbva.elara.domain.transaction.RequestHeaderParamsName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QWAIR001Impl extends QWAIR001Abstract {
	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001.class);
	private static final String CONSULTA_VACIA = "QWAI00841000";
	private static final String CAMPO_INCORRECTO = "QWAI00841001";
	private static final String ERROR_INSERT = "QWAI00841002";
	private QWAIR002 qwaiR002;

	@Override
	public List<CustomerDTO> executeGet(String documentType, int paginationKey, int pagSize) {
		List<Map<String, Object>> mapCustomers;
		List<CustomerDTO> dtoCustomers = null;
		if ((documentType == null) || (documentType.length() == 0)) {
			mapCustomers = jdbcUtils.pagingQueryForList("sql.select_all", paginationKey, pagSize);
		} else {
			String codDocType = qwaiR002.executeDtoToBdDocType(documentType);
			if(!"".equals(codDocType)){
				mapCustomers = jdbcUtils.pagingQueryForList("sql.select_codtipident", paginationKey, pagSize, codDocType);
			}
			else{
				LOGGER.warn("Campo de entrada incorrecto {}",documentType);
				this.addAdvice(CAMPO_INCORRECTO,"documentType");
				return dtoCustomers;
			}
		}
		if (mapCustomers.isEmpty()) {
			LOGGER.warn("No hay registros en la consulta");
			this.addAdvice(CONSULTA_VACIA);
		} else {
			dtoCustomers = new ArrayList<>();
			for (Map<String, Object> map : mapCustomers) {
				CustomerDTO dto = mapToDTOCustomer(map);
				dtoCustomers.add(dto);
			}
			LOGGER.info("Se obtuvieron " + dtoCustomers.size() + " clientes");
		}
		return dtoCustomers;
	}
	
	@Override
	public boolean executePost(CustomerDTO dto, String user, String entity) {
		// Validacion de campos de entrada por sus valores
		
		boolean result = false;
		if (comparadorCampos(dto)) {
			int resultado = this.jdbcUtils.update("sql.insert_customer", dtoToMapCustomer(dto, user, entity));
			if (resultado == 1) {
				LOGGER.info("Cliente registrado en DB");
				result = true;
			} else {
				LOGGER.error("No se registro el cliente en BD");
				this.addAdvice(ERROR_INSERT);
			}
		}
		return result;
	}
	
	@Override
	public CustomerDTO executeGet(String customerId) {
		LOGGER.info("Entramos en la libreria - piruli");
		List<Map<String, Object>> mapCustomers;
		CustomerDTO dtoCustomer = new CustomerDTO();
		dtoCustomer.setCustomerId("11111");
		dtoCustomer.setFirstName("Mer");
		dtoCustomer.setLastName("Paco");
		dtoCustomer.setBirthDate(new Date());
		dtoCustomer.setGenderId("123");
		dtoCustomer.setIdentityDocumentNumber("12345");
		dtoCustomer.setIdentityDocumentType("DNI");
		dtoCustomer.setMaritalStatus("CAS");
		dtoCustomer.setNationality("ES");
		dtoCustomer.setPersonalTitle("SIR");
		
		//Ejemplo recuperar datos de cabecera
		String valor_idioma= (String) this.getRequestHeader().getHeaderParameter(RequestHeaderParamsName.COUNTRYCODE);
		LOGGER.info("Idioma" + valor_idioma);
		
		//Ejemplo traza cogiendo variables de la aplicacion
		String dato_app=this.applicationConfigurationService.getProperty("qwai001.genderId");
		String dato_app_2=this.applicationConfigurationService.getDefaultProperty("qwai001.genderId","Male");
		
		LOGGER.info("dato" + dato_app);
		LOGGER.info("dato" + dato_app_2);
	
		LOGGER.info("LIB: antes de ir a BBDDc - piruli");
		try{
		//Hemos puesto lista porque recuperaba mas de un usuario con ese CustomerID y por
		//tanto no hemos podido utilizar el MAP.
			
		mapCustomers = jdbcUtils.queryForList("sql.select_codpersctpn", customerId);
		LOGGER.info("LIB: despues de ir a BBDDc - piruli");
		LOGGER.info("LIB: Size: " + mapCustomers.size() + " - piruli");
		if(mapCustomers.size() > 1){
			for (Map<String, Object> map : mapCustomers) {
				LOGGER.info("piruli - CUSOMER " + mapToDTOCustomer(map));
			}
		}
		if (mapCustomers.isEmpty()) {
			LOGGER.warn("No hay registros en la consulta - piruli");
			this.addAdvice(CONSULTA_VACIA);
		} else {
			dtoCustomer = mapToDTOCustomer(mapCustomers.get(0));
			LOGGER.info("Se obtuvo 1 cliente - piruli");
		}
		}catch(Exception e){
			LOGGER.info("EXCEPCION - piruli");
			LOGGER.info(e.toString());
		}
		return dtoCustomer;
	}

	private CustomerDTO mapToDTOCustomer(Map<String, Object> map) {
		CustomerDTO dto = new CustomerDTO();
		dto.setNationality(map.get("COD_PAISOALF").toString());
		dto.setIdentityDocumentType(qwaiR002.executeBdToDtoDocType(map.get("COD_TIPIDENT").toString()));
		dto.setIdentityDocumentNumber(map.get("COD_DOCUMPS").toString());
		dto.setPersonalTitle(qwaiR002.executeBdToDtoPersTitle(map.get("COD_TRATANOR").toString()));
		dto.setFirstName(map.get("DES_NOMBFJ").toString());
		dto.setLastName(map.get("DES_APELLUNO").toString() + " " + map.get("DES_APELLDOS"));
		dto.setMaritalStatus(qwaiR002.executeBdToDtoMaritalSt(map.get("COD_CECIVI").toString()));
		dto.setBirthDate((Date) map.get("FEC_FNACIF"));
		dto.setGenderId(qwaiR002.executeBdToDtoGender(map.get("XTI_SEXO").toString()));
		dto.setCustomerId((String) map.get("COD_PERSCTPN"));
		return dto;
	}

	private Map<String, Object> dtoToMapCustomer(CustomerDTO dto, String user, String entity) {
		Map<String, Object> map = new HashMap<>();
		map.put("codPaisoalf", dto.getNationality());
		map.put("codTipident", qwaiR002.executeDtoToBdDocType(dto.getIdentityDocumentType()));
		map.put("codDocumps", dto.getIdentityDocumentNumber());
		map.put("codTratanor", qwaiR002.executeDtoToBdPersTitle(dto.getPersonalTitle()));
		map.put("desNombfj", dto.getFirstName());
		map.put("desApelluno", dto.getLastName().split(" ")[0]);
		map.put("desApelldos", dto.getLastName().split(" ")[1]);
		map.put("codCecivi", qwaiR002.executeDtoToBdMaritalSt(dto.getMaritalStatus()));
		map.put("fecFnacif", dto.getBirthDate());
		map.put("xtiSexo", qwaiR002.executeDtoToBdGender(dto.getGenderId()));
		map.put("codPersctpn", dto.getCustomerId());
		map.put("audUsuario", user);
		map.put("codEntalfa", entity);
		return map;
	}

	private boolean comparadorCampos(CustomerDTO dto) {
		boolean result = false;
		String docType = qwaiR002.executeDtoToBdDocType(dto.getIdentityDocumentType());
		String genderId = qwaiR002.executeDtoToBdGender(dto.getGenderId());
		String personalTitle = qwaiR002.executeDtoToBdPersTitle(dto.getPersonalTitle());
		String maritalStatus = qwaiR002.executeDtoToBdMaritalSt(dto.getMaritalStatus());
		if ("".equals(docType)) {
			this.addAdvice(CAMPO_INCORRECTO, "documentType");
		} else if ("".equals(genderId)) {
			this.addAdvice(CAMPO_INCORRECTO, "genderId");
		} else if ("".equals(personalTitle)) {
			this.addAdvice(CAMPO_INCORRECTO, "personalTitle");
		} else if ("".equals(maritalStatus)) {
			this.addAdvice(CAMPO_INCORRECTO, "maritalStatus");
		} else {
			result = true;
		}
		return result;
	}

	public void setQwaiR002(QWAIR002 qwaiR002) {
		this.qwaiR002 = qwaiR002;
	}
}