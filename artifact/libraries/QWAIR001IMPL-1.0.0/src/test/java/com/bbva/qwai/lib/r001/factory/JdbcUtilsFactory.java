package com.bbva.qwai.lib.r001.factory;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.bbva.qwai.dto.customers.CustomerDTO;
import org.mockito.Mockito;
import org.osgi.framework.BundleContext;

import com.bbva.elara.utility.jdbc.JdbcUtils;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;

public class JdbcUtilsFactory implements
		com.bbva.elara.utility.jdbc.connector.factory.JdbcUtilsFactory {

	@Override
	public JdbcUtils getJdbcUtils(final BundleContext arg0) throws IOException {
		ThreadContext.set(new Context());
		JdbcUtils mock = Mockito.mock(JdbcUtils.class);
		Mockito.when(mock.update("sql.insert_customer", mockPost())).thenReturn(1);
		Mockito.when(mock.pagingQueryForList("sql.select_all", 0, 10)).thenReturn(returnMockGet());
		Mockito.when(mock.pagingQueryForList("sql.select_codartefact", 0, 10, "%,INE,%")).thenReturn(returnMockGet());
		return mock;
	}

	private List<Map<String, Object>> returnMockGet() {
		List<Map<String, Object>> list = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		map.put("COD_ARTEFACT", "MX,INE,9876543210");
		map.put("XTI_APLICACI", "M");
		map.put("COD_IDTEST", "1234567890");
		map.put("DES_TEST", "PEPE,El,Tonelete,Hola");
		map.put("FEC_TEST", Calendar.getInstance().getTime());
		list.add(map);
		return list;
	}

	private Map<String, Object> mockPost() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        CustomerDTO customer = new CustomerDTO();
        customer.setCustomerId("AB12345678");
        customer.setFirstName("Gloria del Carmen");
        customer.setLastName("Garfias Ortiz");
        customer.setMaritalStatus("SINGLE");
        customer.setPersonalTitle("MRS");
        customer.setGenderId("F");
        customer.setNationality("MX");
        customer.setIdentityDocumentType("INE");
        customer.setIdentityDocumentNumber("GAOG123456");
        try {
            customer.setBirthDate(format.parse("1989-02-28"));
        } catch (ParseException e) {

        }
		return dtoToMapCustomer(customer, "MB66646");
	}

	private final static String SEPARADOR = ",";

	private Map<String, Object> dtoToMapCustomer(CustomerDTO dto, String user) {
		Map<String, Object> map = new HashMap<>();
		String artefacto = dto.getNationality() + SEPARADOR + dto.getIdentityDocumentType() + SEPARADOR +
				dto.getIdentityDocumentNumber();
		map.put("codArtefact", artefacto);
		map.put("xtiAplica", dto.getGenderId());
		map.put("codIdtest", dto.getCustomerId());
		String descripcion = dto.getPersonalTitle() + SEPARADOR + dto.getFirstName() + SEPARADOR +
				dto.getLastName() + SEPARADOR + dto.getMaritalStatus();
		map.put("desTest", descripcion);
		map.put("fecTest", dto.getBirthDate());
		map.put("qnuIter", 0);
		map.put("audUsuario", user);
		return map;
	}
}
