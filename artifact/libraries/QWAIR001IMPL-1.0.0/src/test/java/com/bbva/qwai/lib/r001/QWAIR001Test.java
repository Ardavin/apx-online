package com.bbva.qwai.lib.r001;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;

import com.bbva.elara.utility.jdbc.JdbcUtils;
import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.impl.QWAIR001Impl;
import com.bbva.qwai.lib.r002.QWAIR002;
import com.bbva.qwai.mock.QWAIR002Mock;

import junit.framework.Assert;

import com.bbva.elara.domain.transaction.Advice;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;

public class QWAIR001Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIR001.class);

	@InjectMocks
	private QWAIR001Impl qwaiR001;
	
	@Spy
    private Context context;
	
	@Mock
	private JdbcUtils jdbcUtils;
	
	private QWAIR002 qwaiR002 = new QWAIR002Mock();

	@Before
	public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        //Setting current context for Advices testing
        ThreadContext.set(context);
        getObjectIntrospection();
    }

	private Object getObjectIntrospection() throws Exception {
		Object result = this.qwaiR001;
		if (this.qwaiR001 instanceof Advised) {
			Advised advised = (Advised) this.qwaiR001;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}

	@Test
	public void executeGet(){
		LOGGER.info("Ejecucion del test executeGet");
		this.qwaiR001.setQwaiR002(qwaiR002);
		Mockito.doReturn(returnCustomers(null)).when(jdbcUtils).pagingQueryForList("sql.select_all", 0, 10);
		List<CustomerDTO> customers = qwaiR001.executeGet(null, 0, 10);
		LOGGER.info("Busqueda realizada en test get");
		List<Advice> adviceList = context.getAdviceList();
		Assert.assertEquals(0, adviceList.size());
		Assert.assertNotNull(customers);
	}
	@Test
	public void executeGetEmptyString(){
		LOGGER.info("Ejecucion del test executeGetEmptyString");
		this.qwaiR001.setQwaiR002(qwaiR002);
		Mockito.doReturn(returnCustomers(null)).when(jdbcUtils).pagingQueryForList("sql.select_all", 0, 10);
		List<CustomerDTO> customers = qwaiR001.executeGet("", 0, 10);
		LOGGER.info("Busqueda realizada en test get");
		List<Advice> adviceList = context.getAdviceList();
		Assert.assertEquals(0, adviceList.size());
		Assert.assertNotNull(customers);
	}
	@Test
	public void executeGetEmpty(){
		LOGGER.info("Ejecucion del test executeGetEmpty");
		this.qwaiR001.setQwaiR002(qwaiR002);
		Mockito.doReturn(new ArrayList<Map<String, Object>>()).when(jdbcUtils).pagingQueryForList("sql.select_all", -1, 10);
		List<CustomerDTO> customers = qwaiR001.executeGet(null, -1, 10);
		LOGGER.info("Busqueda realizada en test get");
		//ASSERT
        List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841000");
        Assert.assertNull(customers);
	}
	@Test
	public void executeGetQueryParam() {
		LOGGER.info("Ejecucion del test del executeGetQueryParam");
		this.qwaiR001.setQwaiR002(qwaiR002);
		Mockito.doReturn(returnCustomers("CURP")).when(jdbcUtils).pagingQueryForList("sql.select_codtipident", 0, 10,"7");
		List<CustomerDTO> customers = qwaiR001.executeGet("CURP", 0, 10);
		LOGGER.debug("Customer encontrado "+customers.get(0));
		LOGGER.info("Busqueda realizada en test get");
		List<Advice> adviceList = context.getAdviceList();
		Assert.assertEquals(0, adviceList.size());
		Assert.assertNotNull(customers);
	}

	@Test
	public void executeGetQueryParamEmpty() {
		LOGGER.info("Ejecucion del test del executeGetQueryParamEmpty");
		this.qwaiR001.setQwaiR002(qwaiR002);
		Mockito.doReturn(new ArrayList<Map<String, Object>>()).when(jdbcUtils).pagingQueryForList("sql.select_codtipident", 0, 10,"4");
		List<CustomerDTO> customers = qwaiR001.executeGet("CURP", 0, 10);
		LOGGER.info("Busqueda realizada en test get");
		List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841000");
		Assert.assertNull(customers);
	}
	@Test
	public void executeGetQueryParamNoValidParam() {
		LOGGER.info("Ejecucion del test del executeGetQueryParamNoValidParam");
		this.qwaiR001.setQwaiR002(qwaiR002);
		List<CustomerDTO> customers = qwaiR001.executeGet("NVD", 0, 10);
		LOGGER.info("Busqueda realizada en test get");
		List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841001");
		Assert.assertNull(customers);
	}

	@Test
	public void executePost() {
		LOGGER.info("Ejecucion del test executePost");
		CustomerDTO dto = new CustomerDTO();
		dto.setCustomerId("123456789");
		dto.setFirstName("Gloria del Carmen");
		dto.setLastName("Garfias Ortiz");
		dto.setIdentityDocumentType("CURP");
		dto.setIdentityDocumentNumber("GAOG890228MVZLRR08");
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = null;
		try {
			fecha = formato.parse("1989-02-28");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		dto.setBirthDate(fecha);
		dto.setGenderId("FEMALE");
		dto.setPersonalTitle("MISS");
		dto.setNationality("MX");
		dto.setMaritalStatus("SINGLE");
		this.qwaiR001.setQwaiR002(qwaiR002);
		Mockito.doReturn(1).when(jdbcUtils).update("sql.insert_customer", setParamsBD(dto));
		boolean resultado = qwaiR001.executePost(dto, "MB66646", "0182");
		List<Advice> adviceList = context.getAdviceList();
		Assert.assertEquals(0, adviceList.size());
		Assert.assertTrue(resultado);
	}
	@Test
	public void executePostError() {
		LOGGER.info("Ejecucion del test executePostError");
		CustomerDTO dto = new CustomerDTO();
		dto.setCustomerId("12345678910");
		dto.setFirstName("Gloria del Carmen");
		dto.setLastName("Garfias Ortiz");
		dto.setIdentityDocumentType("CURP");
		dto.setIdentityDocumentNumber("GAOG890228MVZLRR08");
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = null;
		try {
			fecha = formato.parse("1989-02-28");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		dto.setBirthDate(fecha);
		dto.setGenderId("FEMALE");
		dto.setPersonalTitle("MISS");
		dto.setNationality("MX");
		dto.setMaritalStatus("SINGLE");
		this.qwaiR001.setQwaiR002(qwaiR002);
		Mockito.doReturn(-1).when(jdbcUtils).update("sql.insert_customer", setParamsBD(dto));
		boolean resultado = qwaiR001.executePost(dto, "MB66646", "0182");
		List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841002");
		Assert.assertFalse(resultado);
	}
	@Test
	public void executePostErrorNoMariatalStatus() {
		LOGGER.info("Ejecucion del test executePostErrorNoMariatalStatus");
		CustomerDTO dto = new CustomerDTO();
		dto.setCustomerId("123456789");
		dto.setFirstName("Gloria del Carmen");
		dto.setLastName("Garfias Ortiz");
		dto.setIdentityDocumentType("CURP");
		dto.setIdentityDocumentNumber("GAOG890228MVZLRR08");
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = null;
		try {
			fecha = formato.parse("1989-02-28");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		dto.setBirthDate(fecha);
		dto.setGenderId("FEMALE");
		dto.setPersonalTitle("MISS");
		dto.setNationality("MX");
		dto.setMaritalStatus("SOLTERA");
		this.qwaiR001.setQwaiR002(qwaiR002);
		boolean resultado = qwaiR001.executePost(dto, "MB66646", "0182");
		List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841001");
		Assert.assertFalse(resultado);
	}
	
	@Test
	public void executePostErrorNoValidGender() {
		LOGGER.info("Ejecucion del test executePostErrorNoValidGender");
		CustomerDTO dto = new CustomerDTO();
		dto.setCustomerId("123456789");
		dto.setFirstName("Gloria del Carmen");
		dto.setLastName("Garfias Ortiz");
		dto.setIdentityDocumentType("CURP");
		dto.setIdentityDocumentNumber("GAOG890228MVZLRR08");
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = null;
		try {
			fecha = formato.parse("1989-02-28");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		dto.setBirthDate(fecha);
		dto.setGenderId("MUJER");
		dto.setPersonalTitle("MISS");
		dto.setNationality("MX");
		dto.setMaritalStatus("SINGLE");
		this.qwaiR001.setQwaiR002(qwaiR002);
		boolean resultado = qwaiR001.executePost(dto, "MB66646", "0182");
		List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841001");
		Assert.assertFalse(resultado);
	}
	@Test
	public void executePostErrorNoValidPersonalTitle() {
		LOGGER.info("Ejecucion del test executePostErrorNoValidPersonalTitle");
		CustomerDTO dto = new CustomerDTO();
		dto.setCustomerId("123456789");
		dto.setFirstName("Gloria del Carmen");
		dto.setLastName("Garfias Ortiz");
		dto.setIdentityDocumentType("CURP");
		dto.setIdentityDocumentNumber("GAOG890228MVZLRR08");
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = null;
		try {
			fecha = formato.parse("1989-02-28");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		dto.setBirthDate(fecha);
		dto.setGenderId("FEMALE");
		dto.setPersonalTitle("SEÑORITA");
		dto.setNationality("MX");
		dto.setMaritalStatus("SINGLE");
		this.qwaiR001.setQwaiR002(qwaiR002);
		boolean resultado = qwaiR001.executePost(dto, "MB66646", "0182");
		List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841001");
		Assert.assertFalse(resultado);
	}

	@Test
	public void executePostErrorNoValidDocumentType() {
		LOGGER.info("Ejecucion del test executePostErrorNoValidDocumentType");
		CustomerDTO dto = new CustomerDTO();
		dto.setCustomerId("123456789");
		dto.setFirstName("Gloria del Carmen");
		dto.setLastName("Garfias Ortiz");
		dto.setIdentityDocumentType("NVD");
		dto.setIdentityDocumentNumber("GAOG890228MVZLRR08");
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = null;
		try {
			fecha = formato.parse("1989-02-28");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		dto.setBirthDate(fecha);
		dto.setGenderId("MUJER");
		dto.setPersonalTitle("SEÑORITA");
		dto.setNationality("MX");
		dto.setMaritalStatus("SINGLE");
		this.qwaiR001.setQwaiR002(qwaiR002);
		boolean resultado = qwaiR001.executePost(dto, "MB66646", "0182");
		List<Advice> adviceList = context.getAdviceList();
        Assert.assertEquals(adviceList.get(0).getCode(), "QWAI00841001");
		Assert.assertFalse(resultado);
	}
	
	private Map<String,Object> setParamsBD(CustomerDTO dto){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("codPaisoalf", dto.getNationality());
		map.put("codTipident", qwaiR002.executeDtoToBdDocType(dto.getIdentityDocumentType()));
		map.put("codDocumps", dto.getIdentityDocumentNumber());
		map.put("codTratanor", qwaiR002.executeDtoToBdPersTitle(dto.getPersonalTitle()));
		map.put("desNombfj", dto.getFirstName());
		map.put("desApelluno", dto.getLastName().split(" ")[0]);
		map.put("desApelldos", dto.getLastName().split(" ")[1]);
		map.put("codCecivi", qwaiR002.executeDtoToBdMaritalSt(dto.getMaritalStatus()));
		map.put("fecFnacif", dto.getBirthDate());
		map.put("xtiSexo", qwaiR002.executeDtoToBdGender(dto.getGenderId()));
		map.put("codPersctpn", dto.getCustomerId());
		map.put("audUsuario", "MB66646");
		map.put("codEntalfa", "0182");
		return map;
	}
	
	private List<Map<String, Object>> returnCustomers(String docType) {
		List<Map<String, Object>> listCustomer = new ArrayList<Map<String, Object>>();
		Map<String, Object> mapCustomer = new HashMap<String, Object>();
		mapCustomer.put("COD_PERSCTPN", "123456789");
		mapCustomer.put("DES_NOMBFJ", "Gloria del Carmen");
		mapCustomer.put("DES_APELLUNO", "Garfias");
		mapCustomer.put("DES_APELLDOS", "Ortiz");
		mapCustomer.put("COD_TIPIDENT", "7");
		mapCustomer.put("COD_DOCUMPS", "GAOG890228MVZLRR08");
		mapCustomer.put("XTI_SEXO", "H");
		mapCustomer.put("COD_TRATANOR", "47");
		mapCustomer.put("COD_CECIVI", "S");
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = null;
		try {
			fecha = formato.parse("1989-02-28");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		mapCustomer.put("FEC_FNACIF", fecha);
		mapCustomer.put("COD_PAISOALF", "MX");
		
		Map<String, Object> mapCustomer2 = new HashMap<String, Object>();
		mapCustomer2.put("COD_PERSCTPN", "A12345678");
		mapCustomer2.put("DES_NOMBFJ", "Marco Antonio");
		mapCustomer2.put("DES_APELLUNO", "Avila");
		mapCustomer2.put("DES_APELLDOS", "Nuñez");
		mapCustomer2.put("COD_TIPIDENT", "9");
		mapCustomer2.put("COD_DOCUMPS", "9876543210");
		mapCustomer2.put("XTI_SEXO", "V");
		mapCustomer2.put("COD_TRATANOR", "15");
		mapCustomer2.put("COD_CECIVI", "S");
		Date fecha2 = null;
		try {
			fecha2 = formato.parse("1987-05-10");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			LOGGER.error("No se pudo parsear la fecha");
		}
		mapCustomer2.put("FEC_FNACIF", fecha2);
		mapCustomer2.put("COD_PAISOALF", "MX");
	
		if(docType==null){
			listCustomer.add(mapCustomer);
			listCustomer.add(mapCustomer2);
		}
		else if(docType.equals("CURP")){
			listCustomer.add(mapCustomer);
		}
		else{
			listCustomer.add(mapCustomer2);
		}
		
		return listCustomer;
	}
	
}