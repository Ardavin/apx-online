package com.bbva.qwai;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.elara.domain.transaction.Severity;
import com.bbva.elara.domain.transaction.response.HttpResponseCode;
import com.bbva.qwai.dto.customers.CustomerDTO;
import com.bbva.qwai.lib.r001.QWAIR001;

/**
 * GET /customers/id
 * Business Logic implementation.
 * @author carlos
 *
 */
public class QWAIT00301MXTransaction extends AbstractQWAIT00301MXTransaction {

	private static final Logger LOGGER = LoggerFactory.getLogger(QWAIT00301MXTransaction.class);
	
	@Override
	public void execute() {
		LOGGER.info("ENTRANDO en la TX! - puruli");
		QWAIR001 qwaiR001 = (QWAIR001)getServiceLibrary(QWAIR001.class);
		// 
		if(QWAIT00301MXAction.GET.name().equals(getRestfulMethod())){
			LOGGER.info("Estamos entrando al metodo GET del API Customers - piruli");
			
			String customerId = getPathParameter("customerId");
			LOGGER.info("TX: tenemos el customerId " + customerId + " - piruli");
			
			CustomerDTO retorno = qwaiR001.executeGet(customerId);
			LOGGER.info("TX: ejecutada la libreria " + retorno + " - piruli");
			
			if (this.getAdvice() != null) {
				if(this.getAdvice().getCode().equals("QWAI00841000")){
					LOGGER.warn("No hubo registros");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_204, Severity.WARN);
				}
				else{
					//QWAI00841002
					LOGGER.error("Hubo un error al encontrar a los clientes");
					setHttpResponseCode(HttpResponseCode.HTTP_CODE_404, Severity.ENR);
				}
			} else {
				LOGGER.info("Busqueda exitosa - piruli");
				setEntity(retorno);
				setContentLocation(getURIPath());
				setHttpResponseCode(HttpResponseCode.HTTP_CODE_200, Severity.OK);
			}
			
			LOGGER.info("Hola somos el Grupo 5 APXXXX - piruli");
			
		}
	}

}
